<?xml version="1.0" encoding="UTF-8"?>
<audio-profiles>
  <profiles>
    <profile id="speex">
      <name>Speex</name>
      <gst-plugin-name>speex</gst-plugin-name>
      <_description>A codec optimized for high quality speech at low bit rate.</_description>
      <output-file-extension>spx</output-file-extension>
      <mimetype>audio/x-speex</mimetype>
      <pipeline>
        <process id="gstreamer-audio">speexenc name=enc</process>
        <process id="multiplexer">
          <value value="oggmux">Merge audio and video streams</value>
		    </process>
        <variable id="bitrate-mode" type="combo">
          <name>Bitrate mode</name>
          <variable-name>vbr</variable-name>
          <default-value>0</default-value>
          <possible-values>
            <_value value="0">Constant bitrate (CBR)</_value>
            <_value value="1">Average bitrate (ABR)</_value>
            <_value value="2">Variable bitrate (VBR)</_value>
          </possible-values>
        </variable>
        <variable id="quality-cbr" type="slider">
          <name>Audio Quality</name>
          <variable-name>quality</variable-name>
          <min-value>0.0</min-value>
          <max-value>10.0</max-value>
          <step-value>1.0</step-value>
          <default-value>8.0</default-value>
        </variable>
        <variable id="quality-vbr" type="slider">
          <name>Audio Quality</name>
          <variable-name>quality</variable-name>
          <min-value>0.0</min-value>
          <max-value>10.0</max-value>
          <step-value>0.1</step-value>
          <default-value>8.0</default-value>
        </variable>
        <variable id="bitrate" type="slider">
          <name>Bitrate</name>
          <variable-name>bitrate</variable-name>
          <min-value>0</min-value>
          <max-value>64</max-value>
          <step-value>1</step-value>
          <default-value>0</default-value>
        </variable>
        <variable id="abr" type="slider">
          <name>Bitrate</name>
          <variable-name>abr</variable-name>
          <min-value>0</min-value>
          <max-value>64</max-value>
          <step-value>1</step-value>
          <default-value>44</default-value>
        </variable>
        <variable id="vbr" type="check">
          <name>Bitrate mode</name>
          <variable-name>vbr</variable-name>
          <default-value>false</default-value>
        </variable>
        <variable id="mode" type="combo">
          <name>Mode</name>
          <_description>Encoding mode</_description>
          <variable-name>mode</variable-name>
          <default-value>0</default-value>
          <possible-values>
            <value value="0">Auto</value>
            <value value="1">Ultra Wide Band</value>
            <value value="2">Wide Band</value>
            <value value="3">Narrow Band</value>
          </possible-values>
        </variable>
        <variable id="complexity" type="slider">
          <name>Complexity</name>
          <_description>Specify the complexity allowed for the encoder. The cpu requirement for a complexity of 10 is about five times higher than for 1.</_description>
          <variable-name>complexity</variable-name>
          <min-value>0.0</min-value>
          <max-value>10.0</max-value>
          <step-value>1.0</step-value>
          <default-value>3.0</default-value>
        </variable>
        <variable id="vad" type="check">
          <name>Voice activity detection</name>
          <_description>Voice activity detection detects non-speech periods and encodes them with just enough bits to reproduce the background noise. Implicitly activated in vbr mode.</_description>
          <variable-name>vad</variable-name>
          <default-value>false</default-value>
        </variable>
        <variable id="dtx" type="check">
          <name>Discontinuous transmission</name>
          <_description>Allows to stop transmitting completely when the background noise is stationary. Non transmission periods are encoded with 5 bits per sample that is equivalent to a bitrate of about 250 bits/s.</_description>
          <variable-name>dtx</variable-name>
          <default-value>false</default-value>
        </variable>
      </pipeline>
    </profile>
  </profiles>
</audio-profiles>
