/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <gst/gst.h>

#include "gnac-main.h"
#include "gnac-options.h"
#include "gnac-settings.h"
#include "libgnac-debug.h"
#include "profiles/gnac-profiles-manager.h"


static gboolean
gnac_options_audio_profile_cb(const gchar  *option_name,
                              const gchar  *value,
                              gpointer      data,
                              GError      **error);

G_GNUC_NORETURN static gboolean
gnac_options_list_profiles_cb(const gchar  *option_name,
                              const gchar  *value,
                              gpointer      data,
                              GError      **error);

static gboolean
gnac_options_debug_cb(const gchar  *option_name,
                      const gchar  *value,
                      gpointer      data,
                      GError      **error);

static gboolean
gnac_options_verbose_cb(const gchar  *option_name,
                        const gchar  *value,
                        gpointer      data,
                        GError      **error);

G_GNUC_NORETURN static gboolean
gnac_options_version_cb(const gchar  *option_name,
                        const gchar  *value,
                        gpointer      data,
                        GError      **error);

GnacCmdLineOptions options;

const GOptionEntry all_options[] = {
  { "audio-profile", 'a',
        0, G_OPTION_ARG_CALLBACK,
        gnac_options_audio_profile_cb,
        N_("Use audio profile 'name'"), N_("name") },
  { "list-profiles", 'l',
        G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
        gnac_options_list_profiles_cb,
        N_("List available profiles and exit"), NULL },
  { "verbose", '\0', 
        G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
        gnac_options_verbose_cb,
        N_("Enable verbose output"), NULL },
  { "debug", '\0',
        G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
        gnac_options_debug_cb,
        N_("Show debugging information"), NULL },
  { "version", '\0', 
        G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, 
        gnac_options_version_cb,
        N_("Show the version of the program and exit"), NULL },
  { G_OPTION_REMAINING, '\0', 
        0, G_OPTION_ARG_FILENAME_ARRAY, 
        &options.filenames, 
        NULL, NULL },
  { NULL }
};


static gboolean
gnac_options_audio_profile_cb(const gchar  *option_name,
                              const gchar  *value,
                              gpointer      data,
                              GError      **error)
{
  gnac_settings_set_string(GNAC_KEY_LAST_USED_PROFILE, value);
  return TRUE;
}


G_GNUC_NORETURN static gboolean
gnac_options_list_profiles_cb(const gchar  *option_name,
                              const gchar  *value,
                              gpointer      data,
                              GError      **error)
{
  gnac_profiles_mgr_list_profiles();
  gnac_exit(EXIT_SUCCESS);
}


static gboolean
gnac_options_debug_cb(const gchar  *option_name,
                      const gchar  *value,
                      gpointer      data,
                      GError      **error)
{
  return gnac_options_enable_debug();
}


static gboolean
gnac_options_verbose_cb(const gchar  *option_name,
                        const gchar  *value,
                        gpointer      data,
                        GError      **error)
{
  return gnac_options_enable_verbose();
}


G_GNUC_NORETURN static gboolean
gnac_options_version_cb(const gchar  *option_name,
                        const gchar  *value,
                        gpointer      data,
                        GError      **error)
{
  g_print("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
  gnac_exit(EXIT_SUCCESS);
}


void
gnac_options_process_filenames(gchar **filenames)
{
  GSList *list_files = NULL; 

  if (filenames) {
    GFile *uri;
    guint  i, len;

    len = g_strv_length(filenames);

    for (i = 0; i < len; i++) {
      uri = g_file_new_for_commandline_arg(filenames[i]);
      list_files = g_slist_prepend(list_files, uri);
    }

    gnac_add_files(list_files);
  }
}


void
gnac_options_init(gint    argc, 
                  gchar **argv)
{
  GError *error = NULL;
  GOptionContext *context;

  /* Translators: message shown in the 'Usage' section when running 'gnac --help' */
  context = g_option_context_new(_("[URI...] - Convert your audio files"));
  g_option_context_add_main_entries(context, all_options, GETTEXT_PACKAGE);
  g_option_context_add_group(context, gst_init_get_option_group());
  g_option_context_add_group(context, gtk_get_option_group(TRUE));

  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    if (error) {
      g_print("%s\n", error->message);
      g_clear_error(&error);
    }
    g_print(_("Run '%s --help' to see a full list "
              "of available command line options."),
              argv[0]);
    g_print("\n");
    gnac_exit(EXIT_FAILURE);
  }

  g_option_context_free(context);
}


void
gnac_options_process_late(void)
{
  gnac_options_process_filenames(options.filenames);
  g_strfreev(options.filenames);
}


gboolean
gnac_options_enable_verbose(void)
{
  gboolean enable = TRUE;
  LIBGNAC_VERBOSE = enable;
  options.verbose = enable;
  return enable;
}


gboolean
gnac_options_enable_debug(void)
{
  gboolean enable = TRUE;
  LIBGNAC_DEBUG = enable;
  options.debug = enable;
  return gnac_options_enable_verbose();
}
