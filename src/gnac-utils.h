/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_UTILS_H
#define GNAC_UTILS_H

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

gchar *
gnac_utils_get_display_name_from_file(GFile   *uri,
                                      GError **error);

gchar *
gnac_utils_get_display_name(const gchar  *uri,
                            GError      **error);

gchar *
gnac_utils_get_mime_type(GFile *uri);

gchar *
gnac_utils_format_duration_for_display(guint64 duration);

guint
gnac_utils_convert_bps_to_kbps(guint value);

guint64 
gnac_utils_moving_avg_add_sample(guint64 sample);

void
gnac_utils_moving_avg_reset(void);

/* The returned value must be freed using g_strfreev() */
gchar **
gnac_utils_get_filenames_from_cmd_line(gchar **argv);

gboolean
gnac_utils_file_format_is_supported(const gchar *mime_type);

gboolean
gnac_utils_string_is_null_or_empty(const gchar *string);

gboolean
gnac_utils_str_equal(const gchar *str1,
                     const gchar *str2);

G_END_DECLS

#endif /* GNAC_UTILS_H */
