/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "gnac-utils.h"
#include "libgnac-debug.h"


static guint64 average = 0;
static guint64 total_samples = 0;


gchar *
gnac_utils_get_display_name_from_file(GFile   *file,
                                      GError **error)
{
  g_return_val_if_fail(!error || !*error, NULL);

  GError *err = NULL;
  GFileInfo *info = g_file_query_info(file,
      G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
      G_FILE_QUERY_INFO_NONE, NULL, &err);
  if (err) {
    g_propagate_error(error, err);
    return NULL;
  }
  
  gchar *display_name = g_strdup(g_file_info_get_display_name(info));

  g_object_unref(info);

  return display_name;
}


gchar *
gnac_utils_get_display_name(const gchar  *uri,
                            GError      **error)
{
  GFile *file = g_file_new_for_uri(uri);
  gchar *display_name = gnac_utils_get_display_name_from_file(file, error);
  g_object_unref(file);
  return display_name;
}


gchar *
gnac_utils_get_mime_type(GFile *file)
{
  GError *error = NULL;
  GFileInfo *info = g_file_query_info(file,
      G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
      G_FILE_QUERY_INFO_NONE, NULL, &error);
  if (error) {
    gchar *uri = g_file_get_uri(file);
    libgnac_debug("Unable to query file info for %s: %s", uri, error->message);
    g_clear_error(&error);
    g_free(uri);
    return NULL;
  }

  gchar *mime_type = g_strdup(g_file_info_get_content_type(info));

  g_object_unref(info);

  return mime_type;
}


gchar *
gnac_utils_format_duration_for_display(guint64 duration)
{
  gint minutes = (gint) ((duration / 60) % 60);
  gint seconds = (gint) (duration % 60);

  if (duration >= 3600) {
    /* Translators: time remaining hours:minutes:seconds. You may change ":" to the separator that your local uses or use "%Id" instead of "%d" if your locale uses localized digits. */
    return g_strdup_printf(_("%d:%02d:%02d"),
        (gint) (duration / 3600), minutes, seconds);
  } else {
    /* Translators: time remaining minutes:seconds. You may change ":" to the separator that your local uses or use "%Id" instead of "%d" if your locale uses localized digits. */
    return g_strdup_printf(_("%d:%02d"), minutes, seconds);
  }

  return NULL;
}


guint
gnac_utils_convert_bps_to_kbps(guint value)
{
  return value / 1000;
}


guint64 
gnac_utils_moving_avg_add_sample(guint64 sample)
{
  total_samples++;
  average = (average * (total_samples-1) + sample) / total_samples;
  return average;
}


void
gnac_utils_moving_avg_reset(void)
{
  average = 0;
  total_samples = 0;
}


/*
 * The returned value must be freed using g_strfreev()
 */
gchar **
gnac_utils_get_filenames_from_cmd_line(gchar **argv)
{
  GPtrArray *array = g_ptr_array_new();

  if (argv) {
    guint i;
    for (i = 0; argv[i]; i++) {
      GFile *file = g_file_new_for_commandline_arg(argv[i]);
      if (file) {
        g_ptr_array_add(array, g_file_get_uri(file));
        g_object_unref(file);
      } else {
        g_printerr("Invalid file name: %s\n", argv[i]);
      }
    }
  }

  g_ptr_array_add(array, NULL);

  return (gchar **) g_ptr_array_free(array, FALSE);
}


gboolean
gnac_utils_file_format_is_supported(const gchar *mime_type)
{
  if (!mime_type) return FALSE;

  return g_str_has_prefix(mime_type, "audio/") ||
         g_str_has_prefix(mime_type, "video/") ||
         g_str_equal(mime_type, "application/ogg") ||
         g_str_equal(mime_type, "application/vnd.rn-realmedia") ||
         g_str_equal(mime_type, "application/x-shockwave-flash");
}


gboolean
gnac_utils_string_is_null_or_empty(const gchar *string)
{
  return !string || gnac_utils_str_equal(string, "");
}


gboolean
gnac_utils_str_equal(const gchar *str1,
                     const gchar *str2)
{
  return g_strcmp0(str1, str2) == 0;
}
