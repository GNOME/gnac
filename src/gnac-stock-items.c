/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "gnac-stock-items.h"


static const GtkStockItem gnac_stock_items[] = {
  { GNAC_STOCK_RESUME, N_("_Resume"), 0, 0, GETTEXT_PACKAGE }
};


static void
gnac_stock_items_register_icons(void)
{
  gtk_stock_add(gnac_stock_items, G_N_ELEMENTS(gnac_stock_items));

  GtkIconFactory *factory = gtk_icon_factory_new();
  gtk_icon_factory_add_default(factory);

  guint i;
  for (i = 0; i < G_N_ELEMENTS(gnac_stock_items); i++) {
    if (g_str_equal(gnac_stock_items[i].stock_id, GNAC_STOCK_RESUME)) {
      GtkIconSet *icon_set;
      icon_set = gtk_icon_factory_lookup_default(GTK_STOCK_MEDIA_PLAY);
      gtk_icon_factory_add(factory, gnac_stock_items[i].stock_id, icon_set);
      gtk_icon_set_unref(icon_set);
    }
  }
}


void
gnac_stock_items_init(void)
{
  gnac_stock_items_register_icons();
}
