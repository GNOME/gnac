/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_UI_H
#define GNAC_UI_H

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "gnac-options.h"

G_BEGIN_DECLS

enum {
  TARGET_STRING
};

extern GSettings *settings_ui;

void
gnac_ui_set_progress_fraction(gdouble fraction);

void
gnac_ui_set_progress_text(const gchar *text);

void
gnac_ui_push_status(const gchar *message);

void
gnac_ui_append_status(const gchar *message);

void
gnac_ui_activate_profiles(gboolean activate);

void
gnac_ui_show_about_dialog(void);

void
gnac_about_url_hook(GtkAboutDialog *dialog,
                    const gchar    *url,
                    gpointer        user_data);

void
gnac_ui_reset_file_filter(void);

void
gnac_ui_on_filter_changed(GtkWidget   *file_chooser,
                          GtkComboBox *combo);

void
gnac_ui_file_chooser_response_cb(GtkDialog *dialog,
                                 gint       response,
                                 gpointer   user_data);

void
gnac_ui_file_chooser_file_activated_cb(GtkFileChooser *chooser,
                                       gpointer        user_data);

gboolean
gnac_ui_file_chooser_key_press_event_cb(GtkWidget   *widget,
                                        GdkEventKey *event,
                                        gpointer     user_data);
void
gnac_ui_init(void);

GtkWidget *
gnac_ui_get_widget(const gchar *widget_name);

GtkAction *
gnac_ui_get_action(const gchar *action_name);

void
gnac_ui_set_action_sensitive(const gchar *action_name,
                             gboolean     activate);

void
gnac_ui_set_action_visible(const gchar *action_name,
                           gboolean     visible);

void
gnac_ui_set_widget_sensitive(const gchar *widget_name,
                             gboolean     activate);

void
gnac_ui_set_widget_visible(const gchar *widget_name,
                           gboolean     visible);

void
gnac_ui_on_audio_empty_state(void);

void
gnac_ui_on_audio_file_action_state(void);

void
gnac_ui_on_audio_ready_state(void);

void
gnac_ui_on_audio_convert_state(void);

gboolean
gnac_ui_on_focus_in_event_cb(GtkWidget     *widget,
                             GdkEventFocus *event,
                             gpointer       data);

void
gnac_ui_on_clear_cb(GtkAction *action,
                    gpointer   data);

void
gnac_ui_on_view_toolbar_cb(GtkAction *action,
                           gpointer   user_data);

void
gnac_ui_on_help_cb(GtkAction *action,
                   gpointer   user_data);

void
gnac_ui_on_properties_cb(GtkAction *action,
                         gpointer   user_data);

void
gnac_ui_on_add_cb(GtkAction *action,
                  gpointer   data);

void
gnac_ui_on_remove_cb(GtkAction *action,
                     gpointer   data);

void
gnac_ui_on_drag_data_received_cb(GtkWidget        *widget,
                                 GdkDragContext   *context,
                                 gint              x,
                                 gint              y,
                                 GtkSelectionData *selection_data,
                                 guint             info,
                                 guint             time,
                                 gpointer          data);

void 
gnac_ui_destroy(void);

gboolean
gnac_ui_confirm_exit(void);

void
gnac_ui_on_conversion_completed(const gchar *msg);

void
gnac_ui_show(void);

void 
gnac_ui_show_popup_menu(GtkWidget      *treeview, 
                        GdkEventButton *event, 
                        gpointer        user_data);

void
gnac_ui_show_trayicon(void);

void
gnac_ui_hide_trayicon(void);

void
gnac_ui_on_trayicon(GtkStatusIcon *trayicon,
                    gpointer       data);

void
gnac_ui_on_trayicon_popup(GtkStatusIcon *trayicon,
                          guint          button,
                          guint          activate_time,
                          gpointer       data);

void
gnac_ui_trayicon_tooltip_update(const gchar *tooltip);

void
gnac_ui_trayicon_menu_activate(gboolean activate);

void
gnac_ui_trayicon_menu_stop(gpointer data, 
                           gpointer user_data);

void
gnac_ui_trayicon_menu_pause(gpointer data, 
                            gpointer user_data);

gint
gnac_ui_show_error_trash(const gchar *filename);

G_END_DECLS

#endif /* GNAC_UI_H */
