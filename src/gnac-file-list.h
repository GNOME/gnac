/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_FILE_LIST_H
#define GNAC_FILE_LIST_H

#include <gio/gio.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

enum {
  COL_STOCK,
  COL_URI,
  COL_DISPLAY,
  COL_TOOLTIP,
  NUM_COLS
};

GtkWidget *
gnac_file_list_new(void);

void
gnac_file_list_attach_default_model(void);

void
gnac_file_list_detach_default_model(void);

void
gnac_file_list_add_row(const gchar *uri);

gboolean 
gnac_file_list_get(GtkTreeRowReference  *reference,
                   gchar               **file_uri);

void 
gnac_file_list_remove_row(const gchar *uri);

void
gnac_file_list_remove_all(void);

guint
gnac_file_list_count_rows(void);

GList *
gnac_file_list_get_selected_rows(void);

gboolean 
gnac_file_list_get_current_row(GtkTreeRowReference **reference);

gboolean 
gnac_file_list_get_next_row(GtkTreeRowReference **reference);

gboolean 
gnac_file_list_get_prev_row(GtkTreeRowReference **reference);

gboolean 
gnac_file_list_has_next_row(GtkTreeRowReference *reference);

gboolean 
gnac_file_list_has_prev_row(GtkTreeRowReference *reference);

void 
gnac_file_list_select_row(GtkTreeRowReference *reference);

void 
gnac_file_list_select_uri(const gchar *uri);

void
gnac_file_list_hide_visual_bar(void);

void
gnac_file_list_visual_error(const gchar *uri, 
                            const gchar *msg,
                            const gchar *stock_item);

void
gnac_file_list_remove_visual_error(const gchar *uri);

void 
gnac_file_list_on_row_activated_cb(GtkTreeView       *treeview,
                                   GtkTreePath       *path,
                                   GtkTreeViewColumn *col,
                                   gpointer           user_data);

gboolean 
gnac_file_list_on_button_pressed(GtkWidget      *treeview, 
                                 GdkEventButton *event, 
                                 gpointer        user_data);

gboolean 
gnac_file_list_popup_menu(GtkWidget *treeview, 
                          gpointer   user_data);

void 
gnac_file_list_delete_key_pressed(GtkWidget   *widget,
                                  GdkEventKey *key,
                                  gpointer     data);

void
gnac_file_list_update_cursor(void);

void  
gnac_file_list_display_uri(GtkTreeViewColumn *tree_column,
                           GtkCellRenderer   *cell,
                           GtkTreeModel      *tree_model,
                           GtkTreeIter       *iter,
                           gpointer           data);

void
gnac_file_list_destroy(void);

G_END_DECLS

#endif /* GNAC_FILE_LIST_H */
