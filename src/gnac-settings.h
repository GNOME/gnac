/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_SETTINGS_H
#define GNAC_SETTINGS_H

#include <gio/gio.h>
#include <glib.h>

#define GNAC_SCHEMA "org.gnome.Gnac.conversion"
#define GNAC_KEY_CLEAR_SOURCE "clear-sources"
#define GNAC_KEY_DESTINATION_DIRECTORY "destination-directory"
#define GNAC_KEY_FOLDER_HIERARCHY "folder-hierarchy"
#define GNAC_KEY_FOLDER_HIERARCHY_PATTERN "folder-hierarchy-pattern"
#define GNAC_KEY_FOLDER_TYPE "folder-type"
#define GNAC_KEY_LAST_USED_PROFILE "last-used-profile"
#define GNAC_KEY_RENAME_PATTERN "rename-pattern"
#define GNAC_KEY_RENAME_PATTERN_PATTERN "rename-pattern-pattern"
#define GNAC_KEY_STRIP_SPECIAL "strip-special"

#define GNAC_SCHEMA_UI "org.gnome.Gnac.ui"
#define GNAC_KEY_TRAY_ICON "tray-icon"
#define GNAC_KEY_TOOLBAR_VISIBLE "toolbar-visible"

G_BEGIN_DECLS

extern GSettings *settings;

void
gnac_settings_init(void);

void
gnac_settings_bind(const gchar *key,
                   gpointer     object);

void
gnac_settings_ui_bind(const gchar *key,
                      gpointer     object);

gboolean
gnac_settings_get_boolean(const gchar *key);

gboolean
gnac_settings_ui_get_boolean(const gchar *key);

gboolean
gnac_settings_set_boolean(const gchar *key,
                          gboolean     value);

gboolean
gnac_settings_ui_set_boolean(const gchar *key,
                             gboolean     value);

gint
gnac_settings_get_int(const gchar *key);

gboolean
gnac_settings_set_int(const gchar *key,
                      gint         value);

gchar *
gnac_settings_get_string(const gchar *key);

gboolean
gnac_settings_set_string(const gchar *key,
                         const gchar *value);

G_END_DECLS

#endif /* GNAC_SETTINGS_H */
