/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gdk/gdkkeysyms.h>
#include <glib/gi18n.h>

#include "gnac-ui-utils.h"
#include "libgnac-debug.h"

#define GNAC_UTILS_ICON_BORDER_WIDTH 1


gint
gnac_ui_utils_get_combo_size(GtkComboBox *combo)
{
  GtkTreeIter iter;
  GtkTreeModel *model = gtk_combo_box_get_model(combo);
  gboolean has_next = gtk_tree_model_get_iter_first(model, &iter);

  guint count = 0;

  while (has_next) {
    count++;
    has_next = gtk_tree_model_iter_next(model, &iter);
  }

  return count;
}


GdkPixbuf *
gnac_ui_utils_scale_pixbuf(GdkPixbuf *pixbuf,
                           gint       new_width,
                           gint       new_height)
{
  g_return_val_if_fail(GDK_IS_PIXBUF(pixbuf), NULL);

  gint n_height, n_width;

  gint width = gdk_pixbuf_get_width(pixbuf);
  gint height = gdk_pixbuf_get_height(pixbuf);

  if (width > height) {
    n_width = new_width;
    n_height = n_width * height / width;
  } else {
    n_height = new_height;
    n_width = n_height * width / height;
  }

  return gdk_pixbuf_scale_simple(pixbuf, n_width, n_height,
      GDK_INTERP_BILINEAR);
}


GdkPixbuf *
gnac_ui_utils_add_border_to_pixbuf(GdkPixbuf *pixbuf)
{
  g_return_val_if_fail(GDK_IS_PIXBUF(pixbuf), NULL);

  gint width = gdk_pixbuf_get_width(pixbuf);
  gint height = gdk_pixbuf_get_height(pixbuf);
  GdkPixbuf *bordered = gdk_pixbuf_new(gdk_pixbuf_get_colorspace(pixbuf),
      gdk_pixbuf_get_has_alpha(pixbuf), gdk_pixbuf_get_bits_per_sample(pixbuf),
      width + (GNAC_UTILS_ICON_BORDER_WIDTH*2),
      height +(GNAC_UTILS_ICON_BORDER_WIDTH*2));
  gdk_pixbuf_fill(bordered, 0xff);
  gdk_pixbuf_copy_area(pixbuf, 0, 0, width, height, bordered, 
      GNAC_UTILS_ICON_BORDER_WIDTH, GNAC_UTILS_ICON_BORDER_WIDTH);
  g_object_unref(pixbuf);
  return bordered;
}


static gboolean
gnac_ui_utils_event_is_double_click(GdkEventButton *event)
{
  return event->type == GDK_2BUTTON_PRESS;
}


static gboolean
gnac_ui_utils_event_is_single_click(GdkEventButton *event)
{
  return event->type == GDK_BUTTON_PRESS;
}


gboolean
gnac_ui_utils_event_is_left_click(GdkEventButton *event)
{
  return event->button == 1;
}


gboolean
gnac_ui_utils_event_is_right_click(GdkEventButton *event)
{
  return event->button == 3;
}


gboolean
gnac_ui_utils_event_is_double_left_click(GdkEventButton *event)
{
  return gnac_ui_utils_event_is_double_click(event) &&
         gnac_ui_utils_event_is_left_click(event);
}


gboolean
gnac_ui_utils_event_is_single_right_click(GdkEventButton *event)
{
  return gnac_ui_utils_event_is_single_click(event) &&
         gnac_ui_utils_event_is_right_click(event);
}


gboolean
gnac_ui_utils_event_is_delete_key(GdkEventKey *event)
{
  return event->keyval == GDK_KEY_Delete;
}


gboolean
gnac_ui_utils_event_is_escape_key(GdkEventKey *event)
{
  return event->keyval == GDK_KEY_Escape;
}


gboolean
gnac_ui_utils_event_is_return_key(GdkEventKey *event)
{
  return event->keyval == GDK_KEY_Return;
}


GtkBuilder *
gnac_ui_utils_create_gtk_builder(const gchar *filename)
{
  GError *error = NULL;
  GtkBuilder *builder = gtk_builder_new();
  gtk_builder_add_from_file(builder, filename, &error);
  if (error) {
    g_printerr("%s: %s\n", _("Unable to read file"), error->message);
    g_clear_error(&error);
  }

  g_assert(!error && builder);

  gtk_builder_connect_signals(builder, NULL);

  return builder;
}


GObject *
gnac_ui_utils_get_object(GtkBuilder  *builder,
                         const gchar *object_name)
{
  GObject *object = gtk_builder_get_object(builder, object_name);
  if (!G_IS_OBJECT(object)) {
    libgnac_debug("Object %s not found\n", object_name);
  }
  return object;
}


GtkWidget *
gnac_ui_utils_get_widget(GtkBuilder  *builder,
                         const gchar *widget_name)
{
  GObject *object = gnac_ui_utils_get_object(builder, widget_name);
  return G_IS_OBJECT(object) ? GTK_WIDGET(object) : NULL;
}


GtkAction *
gnac_ui_utils_get_action(GtkBuilder  *builder,
                         const gchar *action_name)
{
  GObject *object = gnac_ui_utils_get_object(builder, action_name);
  return G_IS_OBJECT(object) ? GTK_ACTION(object) : NULL;
}


void
gnac_ui_utils_set_action_sensitive(GtkBuilder  *builder,
                                   const gchar *action_name,
                                   gboolean     activate)
{
  GtkAction *action = gnac_ui_utils_get_action(builder, action_name);
  if (GTK_IS_ACTION(action)) gtk_action_set_sensitive(action, activate);
}


void
gnac_ui_utils_set_action_visible(GtkBuilder  *builder,
                                 const gchar *action_name,
                                 gboolean     visible)
{
  GtkAction *action = gnac_ui_utils_get_action(builder, action_name);
  if (GTK_IS_ACTION(action)) gtk_action_set_visible(action, visible);
}


void
gnac_ui_utils_set_widget_sensitive(GtkBuilder  *builder,
                                   const gchar *widget_name,
                                   gboolean     activate)
{
  GtkWidget *widget = gnac_ui_utils_get_widget(builder, widget_name);
  if (GTK_IS_WIDGET(widget)) gtk_widget_set_sensitive(widget, activate);
}
