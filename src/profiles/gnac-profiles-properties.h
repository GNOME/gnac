/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_PROPERTIES_H
#define GNAC_PROFILES_PROPERTIES_H

#include <gtk/gtk.h>

#define MAX_DESCR_DISPLAY_SIZE 497
#define MAX_NAME_DISPLAY_SIZE 23

G_BEGIN_DECLS

void
gnac_profiles_properties_init(void);

void
gnac_profiles_properties_set_parent(GtkWindow *parent);

void
gnac_profiles_properties_show(gpointer     profile,
                              const gchar *title,
                              GCallback    call_back);

gpointer
gnac_profiles_properties_load_profile_from_file(const gchar  *path,
                                                const gchar  *filename,
                                                GError      **error);

void
gnac_profiles_properties_free_audio_profile(gpointer profile);

void
gnac_profiles_properties_destroy(void);

gboolean
gnac_profiles_properties_saved_profiles_contain_name(const gchar *name);

void
gnac_profiles_properties_save_profile(gpointer profile);

void
gnac_profiles_properties_on_cancel(GtkWidget *widget,
                                   gpointer   data);

gboolean
gnac_profiles_properties_on_name_entry_key_pressed(GtkWidget   *widget,
                                                   GdkEventKey *event,
                                                   gpointer     data);

gboolean
gnac_profiles_properties_on_window_key_pressed(GtkWidget   *widget,
                                               GdkEventKey *event,
                                               gpointer     data);

gchar *
gnac_profiles_properties_filter_text_for_display(const gchar *text,
                                                 gint         length);

void
gnac_profiles_properties_combo_format_on_changed(GtkComboBox *widget,
                                                 gpointer     user_data);

void
gnac_profiles_properties_update_textbuffer(const gchar *pipeline);

G_END_DECLS

#endif /* GNAC_PROFILES_PROPERTIES_H */
