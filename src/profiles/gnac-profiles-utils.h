/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_UTILS_H
#define GNAC_PROFILES_UTILS_H

#include <glib.h>
#include <gtk/gtk.h>
#include <stdarg.h>

#include "gnac-profiles-xml-engine.h"

#define AUDIO_PROFILE_GET_GENERIC(profile) \
    ((AudioProfileGeneric *)(profile))->generic

G_BEGIN_DECLS

typedef struct _audio_profile_generic
{
  struct _audio_profile_generic *generic;

  gchar    *name;
  gchar    *description;
  gchar    *format_id;
  gchar    *format_name;
  gchar    *extension;
  gchar    *pipeline;
  gchar    *rate;
  gchar    *channels;
  gpointer *user_data;
}
AudioProfileGeneric;

typedef struct
{
  const gchar *gtkbuilder_xml_file;
  GtkBuilder  *builder;
  const gchar *data_xml_file;
  XMLDoc      *doc;
  gchar       *pipeline;
  gchar       *pipeline_encoder;
  gchar       *pipeline_multiplexers;
  gchar       *format_id;
  gchar       *format_plugin_name;
  gchar       *format_name;
  gchar       *file_extension;
  gchar       *description;
}
BasicFormatInfo;

typedef void (*StandardCallBack) (GtkWidget *widget, gpointer data);

gchar *
gnac_profiles_utils_get_combo_format_name(const gchar *name,
                                          const gchar *extension);

GtkWidget *
gnac_profiles_utils_init_base_widget(GtkBuilder  *builder,
                                     XMLDoc      *doc,
                                     const gchar *widget_name,
                                     const gchar *xpath_query);

GtkWidget *
gnac_profiles_utils_init_widget(BasicFormatInfo *bfi,
                                const gchar     *widget_name,
                                const gchar     *xpath_query);

void
gnac_profiles_utils_reset_values(BasicFormatInfo *bfi, ...);

void
gnac_profiles_utils_set_values(BasicFormatInfo *bfi, ...);

void
gnac_profiles_utils_set_values_checked(BasicFormatInfo *bfi, ...);

gchar *
gnac_profiles_utils_get_value_combo(GtkWidget *combo);

void
gnac_profiles_utils_get_values_and_set(BasicFormatInfo *bfi, ...);

void
gnac_profiles_utils_get_values_checked_combo_and_set(BasicFormatInfo *bfi, ...);

void
gnac_profiles_utils_get_values_checked_slider_and_set(BasicFormatInfo *bfi, ...);

void
gnac_profiles_utils_add_description_tooltip(BasicFormatInfo *bfi,
                                            const gchar     *xpath_query,
                                            ...);

void
gnac_profiles_utils_init_raw_audioconvert(XMLDoc *doc);

gchar *
gnac_profiles_utils_get_basepipeline(const gchar *channels,
                                     const gchar *rate);

gchar *
gnac_profiles_utils_add_pipe(gchar       *pipeline,
                             const gchar *new_pipe);

gchar *
gnac_profiles_utils_add_pipes(gchar *pipeline,
                              GList *values);

gchar *
gnac_profiles_utils_add_properties(gchar           *pipeline,
                                   BasicFormatInfo *bfi,
                                   ...);

gchar *
gnac_profiles_utils_add_properties_checked_combo(gchar           *pipeline,
                                                 BasicFormatInfo *bfi,
                                                 ...);

gchar *
gnac_profiles_utils_add_property_slider(gchar       *pipeline,
                                        const gchar *format,
                                        gdouble      factor,
                                        GtkWidget   *slider);

gchar *
gnac_profiles_utils_add_properties_slider(gchar           *pipeline,
                                          BasicFormatInfo *bfi,
                                          const gchar     *format,
                                          ...);

gchar *
gnac_profiles_utils_add_properties_checked_slider(gchar           *pipeline,
                                                  BasicFormatInfo *bfi,
                                                  ...);

void
gnac_profiles_utils_set_active_toggle_button(BasicFormatInfo *bfi,
                                             gboolean         active,
                                             ...);

void
gnac_profiles_utils_on_toggle_optionnal_property(GtkToggleButton *togglebutton,
                                                 gpointer         user_data);

void
gnac_profiles_utils_load_saved_profile(XMLDoc      *doc,
                                       const gchar *base_query,
                                       ...);

AudioProfileGeneric *
gnac_profiles_utils_audio_profile_generic_new(void);

void
gnac_profiles_utils_free_audio_profile_generic(AudioProfileGeneric *profile);

void
gnac_profiles_utils_free_basic_format_info(BasicFormatInfo *bfi);

void
gnac_profiles_utils_free_values(BasicFormatInfo *bfi, ...);

gchar *
gnac_profiles_utils_gdouble_to_gchararray(gdouble value);

gchar *
gnac_profiles_utils_gdouble_to_gchararray_format(gdouble      value,
                                                 const gchar *format);

gdouble
gnac_profiles_utils_gchararray_to_gdouble(const gchar *value);

GtkWidget *
gnac_profiles_utils_get_widget(BasicFormatInfo *bfi,
                               const gchar     *widget_name);

gchar *
gnac_profiles_utils_text_view_get_text(GtkTextView *text_view);

G_END_DECLS

#endif /* GNAC_PROFILES_UTILS_H */
