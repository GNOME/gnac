/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gstdio.h>

#include "gnac-main.h"
#include "gnac-profiles-default.h"
#include "gnac-ui-utils.h"
#include "libgnac-debug.h"


static void
gnac_profiles_default_init_ui(BasicFormatInfo *bfi,
                              XMLDoc          *base_values_doc)
{
  GError *error = NULL;

  bfi->builder = gnac_ui_utils_create_gtk_builder(bfi->gtkbuilder_xml_file);
  if (!gtk_builder_add_from_file(bfi->builder,
      PKGDATADIR "/profiles/gnac-profiles-base-advanced.xml", &error))
  {
    libgnac_critical("%s", error->message);
    g_clear_error(&error);
    gnac_exit(EXIT_FAILURE);
  }

  GtkWidget *profile_advanced_base_widget = gnac_profiles_utils_get_widget(bfi,
      "table-advanced-base");
  GtkWidget *widget = gnac_profiles_utils_get_widget(bfi,
      "properties-table-advanced");
  gtk_grid_attach(GTK_GRID(widget), profile_advanced_base_widget, 0, 0, 2, 1);

  gnac_profiles_utils_init_base_widget(bfi->builder, base_values_doc,
      "combo-channels", "//variable[@id='channels']");
  gnac_profiles_utils_init_base_widget(bfi->builder, base_values_doc,
      "combo-sample-rate", "//variable[@id='sample-rate']");
}


static void
gnac_profiles_default_init_muxers_list(BasicFormatInfo *bfi)
{
  bfi->pipeline_multiplexers = g_strconcat("", NULL);
  GList *multiplexers_list = gnac_profiles_xml_engine_get_list_values(bfi->doc,
      "//process[@id='multiplexer']/value");
  if (multiplexers_list) {
    bfi->pipeline_multiplexers = gnac_profiles_utils_add_pipes(
        bfi->pipeline_multiplexers, multiplexers_list);
    g_list_free(multiplexers_list);
  }
}


void
gnac_profiles_default_init(BasicFormatInfo *bfi)
{
  XMLDoc *base_values_doc = gnac_profiles_xml_engine_load_doc_xpath(
      PKGDATADIR "/profiles/base.xml");

  gnac_profiles_utils_init_raw_audioconvert(base_values_doc);
  
  bfi->doc = gnac_profiles_xml_engine_load_doc_xpath(bfi->data_xml_file);
  bfi->format_id = gnac_profiles_xml_engine_get_format_id(bfi->doc);
  bfi->format_name = gnac_profiles_xml_engine_query_name(bfi->doc,
      bfi->format_id);
  bfi->file_extension = gnac_profiles_xml_engine_query_extension(bfi->doc,
      bfi->format_id);
  bfi->description = gnac_profiles_xml_engine_query_description(bfi->doc,
      bfi->format_id);
  bfi->format_plugin_name = gnac_profiles_xml_engine_get_text_node(bfi->doc,
      "//gst-plugin-name");
  bfi->pipeline_encoder = gnac_profiles_xml_engine_get_text_node(bfi->doc,
      "//process[@id='gstreamer-audio']");

  gnac_profiles_default_init_muxers_list(bfi);

  gnac_profiles_default_init_ui(bfi, base_values_doc);

  gnac_profiles_xml_engine_free_doc_xpath(base_values_doc);
}


GtkWidget *
gnac_profiles_default_get_properties_alignment(BasicFormatInfo *bfi)
{
  return gnac_profiles_utils_get_widget(bfi, "properties-alignment");
}


gchar *
gnac_profiles_default_generate_pipeline(BasicFormatInfo *bfi)
{
  GtkWidget *widget;
  
  widget = gnac_profiles_utils_get_widget(bfi, "combo-channels");
  gchar *channels = gnac_profiles_utils_get_value_combo(widget);
  
  widget = gnac_profiles_utils_get_widget(bfi, "combo-sample-rate");
  gchar *rate = gnac_profiles_utils_get_value_combo(widget);

  gchar *pipeline = gnac_profiles_utils_get_basepipeline(channels, rate);

  g_free(channels);
  g_free(rate);

  pipeline = gnac_profiles_utils_add_pipe(pipeline, bfi->pipeline_encoder);
  
  return pipeline;
}


gchar *
gnac_profiles_default_get_combo_format_name(BasicFormatInfo *bfi)
{
  return gnac_profiles_utils_get_combo_format_name(bfi->format_name,
      bfi->file_extension);
}


void
gnac_profiles_default_reset_ui(BasicFormatInfo *bfi)
{
  gnac_profiles_utils_reset_values(bfi,
      "combo-channels", "combo-sample-rate", NULL);
}


void
gnac_profiles_default_init_fields(AudioProfileGeneric *profile,
                                  BasicFormatInfo     *bfi)
{
  g_return_if_fail(profile);

  gnac_profiles_utils_set_values(bfi,
      "combo-channels", profile->channels,
      "combo-sample-rate", profile->rate, NULL);
}


AudioProfileGeneric *
gnac_profiles_default_generate_audio_profile(BasicFormatInfo *bfi)
{
  AudioProfileGeneric *profile = gnac_profiles_utils_audio_profile_generic_new();

  profile->extension = g_strdup(bfi->file_extension);
  profile->format_id = g_strdup(bfi->format_id);
  profile->format_name = g_strdup(bfi->format_name);
  profile->pipeline = g_strdup(bfi->pipeline);
  profile->channels = gnac_profiles_utils_get_value_combo(
      gnac_profiles_utils_get_widget(bfi, "combo-channels"));
  profile->rate = gnac_profiles_utils_get_value_combo(
      gnac_profiles_utils_get_widget(bfi, "combo-sample-rate"));

  return profile;
}


XMLDoc *
gnac_profiles_default_save_profile(AudioProfileGeneric *profile,
                                   BasicFormatInfo     *bfi)
{
  AudioProfileGeneric *generic = profile->generic;
  XMLDoc *doc = gnac_profiles_xml_engine_load_doc_xpath(
      PKGDATADIR "/profiles/profile-base-save.xml");
  gnac_profiles_xml_engine_modify_values(doc, "/audio-profile/base/*",
      generic->pipeline, generic->rate, generic->channels,
      generic->description, generic->name, generic->extension,
      generic->format_name, generic->format_id, NULL);
  return doc;
}


XMLDoc *
gnac_profiles_default_load_generic_audio_profile(const gchar          *filename,
                                                 AudioProfileGeneric **generic)
{
  XMLDoc *doc = gnac_profiles_xml_engine_load_doc_xpath(filename);
  if (!doc) {
    *generic = NULL;
    return NULL;
  }

  AudioProfileGeneric *profile = gnac_profiles_utils_audio_profile_generic_new();
  *generic = profile;
  gnac_profiles_utils_load_saved_profile(doc, "/audio-profile/base/",
      "format-id", &profile->format_id,
      "format-name", &profile->format_name,
      "format-extension", &profile->extension,
      "name", &profile->name,
      "description", &profile->description,
      "channels", &profile->channels,
      "sample-rate", &profile->rate,
      "pipeline", &profile->pipeline,
      NULL);
  
  return doc;
}


void
gnac_profiles_default_clean_up(BasicFormatInfo *bfi)
{
  GtkWidget *widget;

  widget = gnac_profiles_utils_get_widget(bfi, "table-advanced-base");
  gtk_widget_destroy(widget);

  widget = gnac_profiles_default_get_properties_alignment(bfi);
  gtk_widget_destroy(widget);

  gnac_profiles_utils_free_values(bfi,
      "combo-channels", "combo-sample-rate", NULL);
}
