/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_WAVPACK_H
#define GNAC_PROFILES_WAVPACK_H

#include "../gnac-profiles-default.h"

G_BEGIN_DECLS

FormatModuleFuncs
gnac_profiles_wavpack_get_funcs(void);

void
gnac_profiles_wavpack_generate_pipeline(void);

void
gnac_profiles_wavpack_on_bitrate_control_changed(GtkComboBox *widget,
                                                 gpointer     user_data);

void
gnac_profiles_wavpack_bitrate_control_on_toggle(GtkWidget       *widget,
                                                GtkToggleButton *togglebutton);

void
gnac_profiles_wavpack_joint_stereo_mode_on_toggle(GtkWidget       *widget,
                                                  GtkToggleButton *togglebutton);

G_END_DECLS

#endif /* GNAC_PROFILES_WAVPACK_H */
