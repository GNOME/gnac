/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gstdio.h>

#include "../gnac-profiles-properties.h"
#include "gnac-profiles-lame.h"


typedef struct
{
  AudioProfileGeneric *generic;

  gchar   *encoding_mode;
  gdouble  quality;
  gdouble  compression_ratio;
  gchar   *bitrate;
  gchar   *preset;
  gdouble  mean_abr;
  gdouble  mean_bitrate;
  gchar   *min_bitrate;
  gchar   *max_bitrate;
  gchar   *mode;
}
AudioProfileLame;

typedef enum {
  CONSTANT_BITRATE,
  AVERAGE_BITRATE,
  VARIABLE_BITRATE,
  PRESETS,
  COMPRESSION_RATIO
} EncodingMode;

BasicFormatInfo lame_bfi = {
  PKGDATADIR "/profiles/gnac-profiles-lame.xml",
  NULL,
  PKGDATADIR "/profiles/mp3-lame.xml",
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};


static void
gnac_profiles_lame_vbr_on_changed(GtkComboBox *widget);

static AudioProfileLame *
gnac_profiles_lame_audio_profile_new(void);


void
gnac_profiles_lame_on_encoding_mode_changed(GtkComboBox *widget,
                                            gpointer     user_data)
{
  gnac_profiles_lame_vbr_on_changed(widget);
  gnac_profiles_lame_generate_pipeline();
}


static const gchar *
gnac_profiles_lame_init(void)
{ 
  GtkWidget *widget;
  GtkWidget *widget2;

  gnac_profiles_default_init(&lame_bfi);

  // Bitrate
  gnac_profiles_utils_init_widget(&lame_bfi, "combo-bitrate",
      "//variable[@id='bitrate']");
  
  // Quality
  gnac_profiles_utils_init_widget(&lame_bfi, "slider-vbr-quality",
      "//variable[@id='quality']");

  // Compression ratio
  widget = gnac_profiles_utils_init_widget(&lame_bfi,
      "slider-compression-ratio", "//variable[@id='compression-ratio']");
  widget2 = gnac_profiles_utils_get_widget(&lame_bfi,
      "label-compression-ratio");
  gnac_profiles_utils_add_description_tooltip(&lame_bfi,
      "//variable[@id='compression-ratio']/description",
      widget, widget2, NULL);

  // Preset
  gnac_profiles_utils_init_widget(&lame_bfi, "combo-preset",
      "//variable[@id='preset']");

  // Abr bitrate
  gnac_profiles_utils_init_widget(&lame_bfi, "slider-mean-abr",
      "//variable[@id='mean-bitrate']");
  
  // Mode
  gnac_profiles_utils_init_widget(&lame_bfi, "combo-mode",
      "//variable[@id='mode']");
  
  // Mean bitrate
  gnac_profiles_utils_init_widget(&lame_bfi, "slider-mean-vbr",
      "//variable[@id='mean-bitrate']");
  
  // Min bitrate
  gnac_profiles_utils_init_widget(&lame_bfi, "combo-min-vbr",
      "//variable[@id='min-bitrate']");

  // Max bitrate
  gnac_profiles_utils_init_widget(&lame_bfi, "combo-max-vbr",
      "//variable[@id='max-bitrate']");

  // Encoding mode
  widget = gnac_profiles_utils_init_widget(&lame_bfi, "combo-encoding-mode",
      "//variable[@id='encoding-mode']");
 
  gnac_profiles_lame_vbr_on_changed(GTK_COMBO_BOX(widget));

  gnac_profiles_xml_engine_free_doc_xpath(lame_bfi.doc);
  lame_bfi.doc = NULL;

  return lame_bfi.format_id;
}


static void
gnac_profiles_lame_show_abr_widgets(gboolean show)
{
  GtkWidget *abr_widgets[] = {
    gnac_profiles_utils_get_widget(&lame_bfi, "label-mean-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "slider-mean-abr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "checkbutton-min-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "combo-min-vbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "checkbutton-max-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "combo-max-vbr")
  };

  guint i;
  for (i = 0; i < G_N_ELEMENTS(abr_widgets); i++) {
    if (show) gtk_widget_show_all(abr_widgets[i]);
    else gtk_widget_hide(abr_widgets[i]);
  }
}


static void
gnac_profiles_lame_show_cbr_widgets(gboolean show)
{
  GtkWidget *cbr_widgets[] = {
    gnac_profiles_utils_get_widget(&lame_bfi, "label-cbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "hbox-bitrate"),
  };

  guint i;
  for (i = 0; i < G_N_ELEMENTS(cbr_widgets); i++) {
    if (show) gtk_widget_show_all(cbr_widgets[i]);
    else gtk_widget_hide(cbr_widgets[i]);
  }
}


static void
gnac_profiles_lame_show_vbr_widgets(gboolean show)
{
  GtkWidget *vbr_widgets[] = {
    gnac_profiles_utils_get_widget(&lame_bfi, "label-vbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "hbox-vbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "checkbutton-mean-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "slider-mean-vbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "checkbutton-min-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "combo-min-vbr"),
    gnac_profiles_utils_get_widget(&lame_bfi, "checkbutton-max-bitrate"),
    gnac_profiles_utils_get_widget(&lame_bfi, "combo-max-vbr")
  };

  guint i;
  for (i = 0; i < G_N_ELEMENTS(vbr_widgets); i++) {
    if (show) gtk_widget_show_all(vbr_widgets[i]);
    else gtk_widget_hide(vbr_widgets[i]);
  }
}


static void
gnac_profiles_lame_show_presets_widgets(gboolean show)
{
  GtkWidget *presets_widgets[] = {
    gnac_profiles_utils_get_widget(&lame_bfi, "label-preset"),
    gnac_profiles_utils_get_widget(&lame_bfi, "hbox-preset"),
  };

  guint i;
  for (i = 0; i < G_N_ELEMENTS(presets_widgets); i++) {
    if (show) gtk_widget_show_all(presets_widgets[i]);
    else gtk_widget_hide(presets_widgets[i]);
  }
}


static void
gnac_profiles_lame_show_compression_ratio_widgets(gboolean show)
{
  GtkWidget *compression_ratio_widgets[] = {
    gnac_profiles_utils_get_widget(&lame_bfi, "label-compression-ratio"),
    gnac_profiles_utils_get_widget(&lame_bfi, "slider-compression-ratio"),
  };

  guint i;
  for (i = 0; i < G_N_ELEMENTS(compression_ratio_widgets); i++) {
    if (show) gtk_widget_show_all(compression_ratio_widgets[i]);
    else gtk_widget_hide(compression_ratio_widgets[i]);
  }
}


static void
gnac_profiles_lame_vbr_on_changed(GtkComboBox *widget)
{
  EncodingMode encoding_mode = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));

  gnac_profiles_lame_show_abr_widgets(encoding_mode == AVERAGE_BITRATE);
  gnac_profiles_lame_show_cbr_widgets(encoding_mode == CONSTANT_BITRATE);
  gnac_profiles_lame_show_vbr_widgets(encoding_mode == VARIABLE_BITRATE);
  gnac_profiles_lame_show_presets_widgets(encoding_mode == PRESETS);
  gnac_profiles_lame_show_compression_ratio_widgets(
      encoding_mode == COMPRESSION_RATIO);
}


void
gnac_profiles_lame_generate_pipeline(void)
{
  gchar *pipeline = gnac_profiles_default_generate_pipeline(&lame_bfi);
  GtkWidget *widget = gnac_profiles_utils_get_widget(&lame_bfi,
      "combo-encoding-mode");
  EncodingMode encoding_mode = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));

  // Mode
  pipeline = gnac_profiles_utils_add_properties(pipeline, &lame_bfi,
      "combo-mode", NULL);
  
  switch (encoding_mode)
  {
    case CONSTANT_BITRATE:
      pipeline = gnac_profiles_utils_add_properties(pipeline, &lame_bfi,
          "combo-encoding-mode", "combo-bitrate", NULL);
      break;

    case AVERAGE_BITRATE:
      pipeline = gnac_profiles_utils_add_properties(pipeline, &lame_bfi,
          "combo-encoding-mode", NULL);
      pipeline =  gnac_profiles_utils_add_properties_slider(pipeline, &lame_bfi,
          "%.0f", "slider-mean-abr", NULL);
      pipeline =  gnac_profiles_utils_add_properties_checked_combo(pipeline,
          &lame_bfi,
          "combo-min-vbr", "checkbutton-min-bitrate",
          "combo-max-vbr", "checkbutton-max-bitrate",
          NULL);
      break;

    case VARIABLE_BITRATE:
      pipeline = gnac_profiles_utils_add_properties(pipeline, &lame_bfi,
          "combo-encoding-mode", NULL);
      pipeline = gnac_profiles_utils_add_properties_slider(pipeline, &lame_bfi, 
          "%.0f", "slider-vbr-quality", NULL);
      pipeline =  gnac_profiles_utils_add_properties_checked_slider(pipeline,
          &lame_bfi, "slider-mean-vbr", "checkbutton-mean-bitrate", NULL);
      pipeline =  gnac_profiles_utils_add_properties_checked_combo(pipeline,
          &lame_bfi,
          "combo-min-vbr", "checkbutton-min-bitrate",
          "combo-max-vbr", "checkbutton-max-bitrate",
          NULL);
      break;

    case PRESETS:
      pipeline = gnac_profiles_utils_add_properties(pipeline, &lame_bfi,
          "combo-preset", NULL);
      break;

    case COMPRESSION_RATIO:
      pipeline = gnac_profiles_utils_add_properties_slider(pipeline, &lame_bfi,
          "%.0f", "slider-compression-ratio", NULL);
      break;

    default:
      break;
  }

  pipeline = gnac_profiles_utils_add_pipe(pipeline,
      lame_bfi.pipeline_multiplexers);
  gnac_profiles_properties_update_textbuffer(pipeline);

  g_free(lame_bfi.pipeline);

  lame_bfi.pipeline = pipeline;
}


void
gnac_profiles_lame_advanced_bitrate_on_toggle(GtkWidget       *widget,
                                              GtkToggleButton *togglebutton)
{
  gnac_profiles_utils_on_toggle_optionnal_property(togglebutton, widget);
  gnac_profiles_lame_generate_pipeline();
}


static void
gnac_profiles_lame_reset_ui(void)
{
  gnac_profiles_utils_reset_values(&lame_bfi,
      "combo-mode", "combo-bitrate", "combo-min-vbr", "combo-max-vbr",
      "combo-preset", "combo-encoding-mode", "slider-vbr-quality",
      "slider-compression-ratio", "slider-mean-vbr", "slider-mean-abr",
      NULL);
  gnac_profiles_utils_set_active_toggle_button(&lame_bfi, FALSE,
      "checkbutton-mean-bitrate", "checkbutton-min-bitrate",
      "checkbutton-max-bitrate", NULL);
}


static void
gnac_profiles_lame_set_fields(gpointer data)
{
  if (!data) {
    gnac_profiles_lame_reset_ui();
    return;
  }

  AudioProfileLame *profile = (AudioProfileLame *) data;
  gnac_profiles_utils_set_values(&lame_bfi,
      "combo-encoding-mode", profile->encoding_mode,
      "combo-mode", profile->mode, NULL);

  if (profile->bitrate)
  {
    gnac_profiles_utils_set_values(&lame_bfi,
        "combo-bitrate", profile->bitrate, NULL);
  }
  else if (profile->preset)
  {
    gnac_profiles_utils_set_values(&lame_bfi,
        "combo-preset", profile->preset, NULL);
  }
  else if (profile->mean_abr != -1)
  {
    gnac_profiles_utils_set_values(&lame_bfi,
        "slider-mean-abr", profile->mean_abr, NULL);
    gnac_profiles_utils_set_values_checked(&lame_bfi,
        "combo-min-vbr", "checkbutton-min-bitrate", profile->min_bitrate,
        "combo-max-vbr", "checkbutton-max-bitrate", profile->max_bitrate,
        NULL);
  }
  else if (profile->quality != -1)
  {
    gnac_profiles_utils_set_values(&lame_bfi,
        "slider-vbr-quality", profile->quality, NULL);
    gnac_profiles_utils_set_values_checked(&lame_bfi,
        "combo-min-vbr", "checkbutton-min-bitrate", profile->min_bitrate,
        "combo-max-vbr", "checkbutton-max-bitrate", profile->max_bitrate,
        "slider-mean-vbr", "checkbutton-mean-bitrate", profile->mean_bitrate,
        NULL);
  }
  else if (profile->compression_ratio != -1)
  {
    gnac_profiles_utils_set_values(&lame_bfi,
        "slider-compression-ratio", profile->compression_ratio, NULL);
  }
}


static gchar *
gnac_profiles_lame_get_combo_format_name(void)
{
  return gnac_profiles_default_get_combo_format_name(&lame_bfi);
}


static AudioProfileLame *
gnac_profiles_lame_audio_profile_new(void)
{
  AudioProfileLame *profile = g_malloc(sizeof(AudioProfileLame));
  
  profile->encoding_mode = NULL;
  profile->mode = NULL;
  profile->bitrate = NULL;
  profile->preset = NULL;
  profile->quality = -1;
  profile->compression_ratio = -1;
  profile->mean_bitrate = -1;
  profile->mean_abr = -1;
  profile->min_bitrate = NULL;
  profile->max_bitrate = NULL;

  return profile;
}


static void
gnac_profiles_lame_free_audio_profile(gpointer data)
{
  if (!data) return;

  AudioProfileLame *profile = (AudioProfileLame *) data;
  gnac_profiles_utils_free_audio_profile_generic(profile->generic); 
  g_free(profile->encoding_mode);
  g_free(profile->mode);
  g_free(profile->bitrate);
  g_free(profile->min_bitrate);
  g_free(profile->max_bitrate);
  g_free(profile->preset);
  g_free(profile);
}


static gpointer
gnac_profiles_lame_generate_audio_profile(GError **error)
{
  AudioProfileGeneric *generic = gnac_profiles_default_generate_audio_profile(
      &lame_bfi);
  AudioProfileLame *profile = gnac_profiles_lame_audio_profile_new();
  profile->generic = generic;
  
  gnac_profiles_utils_get_values_and_set(&lame_bfi,
      "combo-encoding-mode", &profile->encoding_mode,
      "combo-mode", &profile->mode,
      NULL);

  GtkWidget *widget = gnac_profiles_utils_get_widget(&lame_bfi,
      "combo-encoding-mode");

  switch (gtk_combo_box_get_active(GTK_COMBO_BOX(widget)))
  {
    case CONSTANT_BITRATE:
      gnac_profiles_utils_get_values_and_set(&lame_bfi,
          "combo-bitrate", &profile->bitrate, NULL);
      break;

    case AVERAGE_BITRATE:
      gnac_profiles_utils_get_values_and_set(&lame_bfi,
          "slider-mean-abr", &profile->mean_abr, NULL);
      gnac_profiles_utils_get_values_checked_combo_and_set(&lame_bfi,
          "combo-min-vbr", "checkbutton-min-bitrate", &profile->min_bitrate,
          "combo-max-vbr", "checkbutton-max-bitrate", &profile->max_bitrate,
          NULL );
      break;

    case VARIABLE_BITRATE:
      gnac_profiles_utils_get_values_and_set(&lame_bfi,
          "slider-vbr-quality", &profile->quality, NULL);
      gnac_profiles_utils_get_values_checked_slider_and_set(&lame_bfi,
          "slider-mean-vbr", "checkbutton-mean-bitrate", &profile->mean_bitrate,
          NULL);
      gnac_profiles_utils_get_values_checked_combo_and_set(&lame_bfi,
          "combo-min-vbr", "checkbutton-min-bitrate", &profile->min_bitrate,
          "combo-max-vbr", "checkbutton-max-bitrate", &profile->max_bitrate,
          NULL );
      break;

    case PRESETS:
      gnac_profiles_utils_get_values_and_set(&lame_bfi,
          "combo-preset", &profile->preset, NULL);
      break;

    case COMPRESSION_RATIO:
      gnac_profiles_utils_get_values_and_set(&lame_bfi,
          "slider-compression-ratio", &profile->compression_ratio, NULL);
      break;

    default:
      break;
  }

  return profile;
}


static GtkWidget *
gnac_profiles_lame_get_widget(void)
{
  return gnac_profiles_default_get_properties_alignment(&lame_bfi);
}


static void
gnac_profiles_lame_save_profile(gpointer data)
{
  if (!data) return;

  gchar *quality = NULL;
  gchar *mean_bitrate = NULL;
  gchar *mean_abr = NULL;
  gchar *compression_ratio = NULL;
  AudioProfileLame *profile = (AudioProfileLame *) data;
  
  if (profile->quality != -1)
  {
    quality = gnac_profiles_utils_gdouble_to_gchararray(profile->quality);

    if (profile->mean_bitrate != -1) {
      mean_bitrate = gnac_profiles_utils_gdouble_to_gchararray(
          profile->mean_bitrate);
    }
  }
  else if (profile->mean_abr != -1)
  {
    mean_abr = gnac_profiles_utils_gdouble_to_gchararray(profile->mean_abr);
  }
  else if (profile->compression_ratio != -1) {
    compression_ratio = gnac_profiles_utils_gdouble_to_gchararray(
        profile->compression_ratio);
  }

  XMLDoc *doc = gnac_profiles_default_save_profile(profile->generic, &lame_bfi);
  gnac_profiles_xml_engine_add_values(doc,
      "encoding-mode", profile->encoding_mode,
      "bitrate", profile->bitrate,
      "quality", quality,
      "compression-ratio", compression_ratio,
      "preset", profile->preset,
      "mean-abr", mean_abr,
      "mean-bitrate", mean_bitrate,
      "min-bitrate", profile->min_bitrate,
      "max-bitrate", profile->max_bitrate,
      "mode", profile->mode,
      NULL);

  gnac_profiles_xml_engine_save_doc(doc, profile->generic->name);

  gnac_profiles_xml_engine_free_doc_xpath(doc);
  g_free(quality);
  g_free(mean_bitrate);
  g_free(compression_ratio);
}


static gpointer
gnac_profiles_lame_load_specific_properties(XMLDoc              *doc,
                                            AudioProfileGeneric *generic)
{
  gchar *quality;
  gchar *mean_bitrate;
  gchar *mean_abr;
  gchar *compression_ratio;
  
  AudioProfileLame *profile = gnac_profiles_lame_audio_profile_new();
  profile->generic = generic;
  gnac_profiles_utils_load_saved_profile(doc,
      "/audio-profile/format-specific/",
      "encoding-mode", &profile->encoding_mode,
      "bitrate",  &profile->bitrate,
      "quality",  &quality,
      "compression-ratio",  &compression_ratio,
      "preset", &profile->preset,
      "mean-abr",  &mean_abr,
      "mean-bitrate",  &mean_bitrate,
      "min-bitrate",  &profile->min_bitrate,
      "max-bitrate",  &profile->max_bitrate,
      "mode", &profile->mode,
      NULL);

  if (quality) {
    profile->quality = gnac_profiles_utils_gchararray_to_gdouble(quality);
    g_free(quality);
    
    if (mean_bitrate) {
      profile->mean_bitrate = gnac_profiles_utils_gchararray_to_gdouble(
          mean_bitrate);
      g_free(mean_bitrate);
    }
  } else if (mean_abr) {
    profile->mean_abr = gnac_profiles_utils_gchararray_to_gdouble(mean_abr);
    g_free(mean_abr);
  } else if (compression_ratio) {
    profile->compression_ratio = gnac_profiles_utils_gchararray_to_gdouble(
        compression_ratio);
    g_free(compression_ratio);
  }

  return profile;
}


static void
gnac_profiles_lame_clean_up(void)
{
  gnac_profiles_default_clean_up(&lame_bfi);
  gnac_profiles_utils_free_values(&lame_bfi,
      "combo-mode", "combo-bitrate", "combo-min-vbr", "combo-max-vbr",
      "combo-preset", "combo-encoding-mode", "slider-vbr-quality",
      "slider-compression-ratio", "slider-mean-vbr", "slider-mean-abr",
      NULL);
  gnac_profiles_utils_free_basic_format_info(&lame_bfi);
}


static const gchar *
gnac_profiles_lame_get_plugin_name(void)
{
  return lame_bfi.format_plugin_name;
}


static const gchar *
gnac_profiles_lame_get_description(void)
{
  return lame_bfi.description;
}


FormatModuleFuncs
gnac_profiles_lame_get_funcs(void)
{
  FormatModuleFuncs funcs = {
    gnac_profiles_lame_init,
    gnac_profiles_lame_get_description,
    gnac_profiles_lame_generate_pipeline,
    gnac_profiles_lame_generate_audio_profile,
    gnac_profiles_lame_free_audio_profile,
    gnac_profiles_lame_set_fields,
    gnac_profiles_lame_get_widget,
    gnac_profiles_lame_save_profile,
    gnac_profiles_lame_load_specific_properties,
    gnac_profiles_lame_clean_up,
    NULL,
    gnac_profiles_lame_get_combo_format_name,
    gnac_profiles_lame_get_plugin_name
  };

  return funcs;
}
