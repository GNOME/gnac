/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_DEFAULT_H
#define GNAC_PROFILES_DEFAULT_H

#include "gnac-profiles-utils.h"

G_BEGIN_DECLS

typedef struct {
  const gchar *(*init)                    (void);
  const gchar *(*get_description)         (void);
  void         (*generate_pipeline)       (void);
  gpointer     (*generate_audio_profile)  (GError **);
  void         (*free_audio_profile)      (gpointer);
  void         (*set_fields)              (gpointer);
  GtkWidget   *(*get_widget)              (void);
  void         (*save_profile_in_file)    (gpointer);
  gpointer     (*load_specific_properties)(XMLDoc *, AudioProfileGeneric *);
  void         (*clean_up)                (void);
  void         (*set_pipeline_func)       (GtkTextView *, GtkWidget *);
  gchar       *(*get_combo_format_name)   (void);
  const gchar *(*get_plugin_name)         (void);
} FormatModuleFuncs;

typedef FormatModuleFuncs (*FormatModuleGetFuncs) (void);

void
gnac_profiles_default_init(BasicFormatInfo *bfi);

GtkWidget *
gnac_profiles_default_get_properties_alignment(BasicFormatInfo *bfi);

gchar *
gnac_profiles_default_get_combo_format_name(BasicFormatInfo *bfi);

gchar *
gnac_profiles_default_generate_pipeline(BasicFormatInfo *bfi);

void
gnac_profiles_default_reset_ui(BasicFormatInfo *bfi);

void
gnac_profiles_default_init_fields(AudioProfileGeneric *profile,
                                  BasicFormatInfo     *bfi);

AudioProfileGeneric *
gnac_profiles_default_generate_audio_profile(BasicFormatInfo *bfi);

XMLDoc *
gnac_profiles_default_save_profile(AudioProfileGeneric *profile,
                                   BasicFormatInfo     *bfi);

XMLDoc *
gnac_profiles_default_load_generic_audio_profile(const gchar        *filename,
                                               AudioProfileGeneric **generic);

void
gnac_profiles_default_clean_up(BasicFormatInfo *bfi);

G_END_DECLS

#endif /* GNAC_PROFILES_DEFAULT_H */
