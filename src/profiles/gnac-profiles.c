/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "gnac-main.h"
#include "gnac-profiles.h"
#include "gnac-profiles-manager.h"
#include "gnac-settings.h"
#include "gnac-ui.h"
#include "gnac-utils.h"


enum {
  COL_NAME,
  COL_PROFILE,
  NUM_COLS
};


void
gnac_profiles_init(void)
{
  gnac_profiles_mgr_init();
  gnac_profiles_populate_combo();
}


static AudioProfileGeneric *
gnac_profiles_get_active_profile(void)
{
  GtkTreeIter iter;
  GtkComboBox *combo_profile = GTK_COMBO_BOX(gnac_ui_get_widget("combo_profile"));
  GtkTreeModel *model = gtk_combo_box_get_model(combo_profile);

  if (gtk_combo_box_get_active_iter(combo_profile, &iter)) {
    AudioProfileGeneric *profile;
    gtk_tree_model_get(model, &iter, COL_PROFILE, &profile, -1);
    return profile->generic;
  }

  return NULL;
}


const gchar *
gnac_profiles_get_pipeline(void)
{
  AudioProfileGeneric *profile = gnac_profiles_get_active_profile();
  if (!profile) return NULL;

  return profile->pipeline;
}


const gchar *
gnac_profiles_get_extension(void)
{
  AudioProfileGeneric *profile = gnac_profiles_get_active_profile();
  if (!profile) return NULL;

  return profile->extension;
}


const gchar *
gnac_profiles_get_name(void)
{ 
  AudioProfileGeneric *profile = gnac_profiles_get_active_profile();
  if (!profile) return NULL;

  return profile->name;
}


static void
gnac_profiles_select_last_used_profile(void)
{
  GtkTreeIter iter;
  GtkComboBox *combo_profile = GTK_COMBO_BOX(gnac_ui_get_widget("combo_profile"));
  GtkTreeModel *model = gtk_combo_box_get_model(combo_profile);
  gboolean has_next = gtk_tree_model_get_iter_first(model, &iter);

  gboolean found = FALSE;
  gchar *last_used_profile = gnac_settings_get_string(
      GNAC_KEY_LAST_USED_PROFILE);

  while (has_next && !found) {
    AudioProfileGeneric *profile;
    gtk_tree_model_get(model, &iter, COL_PROFILE, &profile, -1);
    if (gnac_utils_str_equal(profile->name, last_used_profile)) {
      gtk_combo_box_set_active_iter(combo_profile, &iter);
      found = TRUE;
    }
    has_next = gtk_tree_model_iter_next(model, &iter);
  }

  g_free(last_used_profile);

  if (!found && gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter)) {
    gtk_combo_box_set_active_iter(GTK_COMBO_BOX(combo_profile), &iter);
  }
}


void
gnac_profiles_populate_combo(void)
{
  GtkWidget *combo_profile = gnac_ui_get_widget("combo_profile");
  GtkListStore *model = GTK_LIST_STORE(
      gtk_combo_box_get_model(GTK_COMBO_BOX(combo_profile)));
 
  GList *profiles = gnac_profiles_mgr_get_profiles_list();
  if (!profiles) {
    gtk_widget_set_sensitive(combo_profile, FALSE);
    return;
  }

  gtk_widget_set_sensitive(combo_profile, TRUE);
  gtk_list_store_clear(model);

  GList *temp;
  GtkTreeIter iter;

  for (temp = profiles; temp; temp = g_list_next(temp)) {
    AudioProfileGeneric *profile = AUDIO_PROFILE_GET_GENERIC(temp->data);
    gchar *formatted_name = gnac_profiles_properties_filter_text_for_display(
        profile->name, MAX_NAME_DISPLAY_SIZE);
    gchar *name = gnac_profiles_utils_get_combo_format_name(formatted_name,
        profile->extension);
    g_free(formatted_name);
    gtk_list_store_append(model, &iter);
    gtk_list_store_set(model, &iter, COL_NAME, name, COL_PROFILE, profile, -1);
    g_free(name);
  }

  g_list_free(profiles);

  gnac_profiles_select_last_used_profile();
}


void
gnac_profiles_on_combo_profile_changed(GtkWidget *widget,
                                       gpointer   data)
{
  AudioProfileGeneric *profile = gnac_profiles_get_active_profile();
  if (!profile) return;

  const gchar *profile_name = gnac_profiles_get_name();
  gnac_settings_set_string(GNAC_KEY_LAST_USED_PROFILE, profile_name);

  gchar *description;

  if (gnac_utils_string_is_null_or_empty(profile->description)) {
    description = g_strdup(_("No description available"));
  } else {
    gchar *formatted = gnac_profiles_properties_filter_text_for_display(
        profile->description, MAX_DESCR_DISPLAY_SIZE);
    description = gnac_profiles_xml_engine_format_text_to_xml(formatted);
    g_free(formatted);
  }

  gtk_widget_set_tooltip_markup(widget, description);

  g_free(description);
}
