/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_UI_UTILS_H
#define GNAC_UI_UTILS_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

gint
gnac_ui_utils_get_combo_size(GtkComboBox *combo);

/* scale pixbuf to width x height while keeping the aspect ratio */
GdkPixbuf *
gnac_ui_utils_scale_pixbuf(GdkPixbuf *pixbuf,
                           gint       width,
                           gint       height);

/* add a one pixel black border around pixbuf */
GdkPixbuf *
gnac_ui_utils_add_border_to_pixbuf(GdkPixbuf *pixbuf);

gboolean
gnac_ui_utils_event_is_left_click(GdkEventButton *event);

gboolean
gnac_ui_utils_event_is_right_click(GdkEventButton *event);

gboolean
gnac_ui_utils_event_is_double_left_click(GdkEventButton *event);

gboolean
gnac_ui_utils_event_is_single_right_click(GdkEventButton *event);

gboolean
gnac_ui_utils_event_is_key_press(GdkEventKey *event);

gboolean
gnac_ui_utils_event_is_delete_key(GdkEventKey *event);

gboolean
gnac_ui_utils_event_is_escape_key(GdkEventKey *event);

gboolean
gnac_ui_utils_event_is_return_key(GdkEventKey *event);

GtkBuilder *
gnac_ui_utils_create_gtk_builder(const gchar *filename);

GObject *
gnac_ui_utils_get_object(GtkBuilder  *builder,
                         const gchar *object_name);

GtkWidget *
gnac_ui_utils_get_widget(GtkBuilder  *builder,
                         const gchar *widget_name);

GtkAction *
gnac_ui_utils_get_action(GtkBuilder  *builder,
                         const gchar *action_name);

void
gnac_ui_utils_set_action_sensitive(GtkBuilder  *builder,
                                   const gchar *action_name,
                                   gboolean     activate);

void
gnac_ui_utils_set_action_visible(GtkBuilder  *builder,
                                 const gchar *action_name,
                                 gboolean     visible);

void
gnac_ui_utils_set_widget_sensitive(GtkBuilder  *builder,
                                   const gchar *widget_name,
                                   gboolean     activate);

G_END_DECLS

#endif /* GNAC_UI_UTILS_H */
