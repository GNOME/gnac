/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_MEDIA_ITEM_H__
#define __LIBGNAC_MEDIA_ITEM_H__

#include <gio/gio.h>
#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

#include "libgnac-profile.h"

G_BEGIN_DECLS

typedef struct 
{
  gpointer        parent;
  GFile          *source;
  GFile          *destination;
  LibgnacProfile *profile;
  GstElement     *pipeline;
  GstElement     *audio_encoder;
  GstElement     *muxer;
  GstElement     *video_encoder;
  GstBus         *bus;
  gint            video_channel;
  guint           timeout_id;
  gint64          pos;
} LibgnacMediaItem;


LibgnacMediaItem *
libgnac_item_new(GFile          *source,
                 LibgnacProfile *profile,
                 gpointer        parent);

gboolean
libgnac_item_has_audio(LibgnacMediaItem *item);

gboolean
libgnac_item_has_video(LibgnacMediaItem *item);

void
libgnac_item_run(LibgnacMediaItem  *item, 
                 GError           **error);

void
libgnac_item_stop(LibgnacMediaItem  *item, 
                  GError           **error);

void
libgnac_item_pause(LibgnacMediaItem  *item, 
                   GError           **error);

void
libgnac_item_resume(LibgnacMediaItem  *item, 
                    GError           **error);

void
libgnac_item_free(LibgnacMediaItem *item);

void
libgnac_item_init_event_src(LibgnacMediaItem *item);

void
libgnac_item_clear_event_src(LibgnacMediaItem *item);

/* Callbacks */

void
libgnac_item_eos_cb(GstBus    *bus, 
                   GstMessage *message, 
                   gpointer    data);

void
libgnac_item_error_cb(GstBus    *bus, 
                     GstMessage *message, 
                     gpointer    data);

G_END_DECLS

#endif /* __LIBGNAC_MEDIA_ITEM_H__ */
