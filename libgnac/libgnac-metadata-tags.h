/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_METADATA_TAGS_H__
#define __LIBGNAC_METADATA_TAGS_H__

#include <glib.h>

G_BEGIN_DECLS

typedef GHashTable LibgnacTags;

/* Non official GStreamer tags */
#define GNAC_TAG_CHANNELS    "channels"
#define GNAC_TAG_AUDIO_DEPTH "audio-depth"
#define GNAC_TAG_FILENAME    "filename"
#define GNAC_TAG_FILE_SIZE   "file-size"
#define GNAC_TAG_FRAMERATE   "framerate"
#define GNAC_TAG_HAS_AUDIO   "has-audio"
#define GNAC_TAG_HAS_VIDEO   "has-video"
#define GNAC_TAG_HEIGHT      "height"
#define GNAC_TAG_INTERLACED  "interlaced"
#define GNAC_TAG_MODE        "channel-mode"
#define GNAC_TAG_PAR         "par"
#define GNAC_TAG_RATE        "rate"
#define GNAC_TAG_VBR         "vbr"
#define GNAC_TAG_VIDEO_DEPTH "video-depth"
#define GNAC_TAG_WIDTH       "width"

#define LIBGNAC_METADATA_TAG_IS_ALBUM(tag) \
    (g_str_equal((tag), GST_TAG_ALBUM))
#define LIBGNAC_METADATA_TAG_IS_ALBUM_GAIN(tag) \
    (g_str_equal((tag), GST_TAG_ALBUM_GAIN))
#define LIBGNAC_METADATA_TAG_IS_ALBUM_PEAK(tag) \
    (g_str_equal((tag), GST_TAG_ALBUM_PEAK))
#define LIBGNAC_METADATA_TAG_IS_ALBUM_VOLUME_COUNT(tag) \
    (g_str_equal((tag), GST_TAG_ALBUM_VOLUME_COUNT))
#define LIBGNAC_METADATA_TAG_IS_ALBUM_VOLUME_NUMBER(tag) \
    (g_str_equal((tag), GST_TAG_ALBUM_VOLUME_NUMBER))
#define LIBGNAC_METADATA_TAG_IS_ARTIST(tag) \
    (g_str_equal((tag), GST_TAG_ARTIST))
#define LIBGNAC_METADATA_TAG_IS_AUDIO_CODEC(tag) \
    (g_str_equal((tag), GST_TAG_AUDIO_CODEC))
#define LIBGNAC_METADATA_TAG_IS_AUDIO_DEPTH(tag) \
    (g_str_equal((tag), GST_TAG_AUDIO_DEPTH))
#define LIBGNAC_METADATA_TAG_IS_BITRATE(tag) \
    (g_str_equal((tag), GST_TAG_BITRATE))
#define LIBGNAC_METADATA_TAG_IS_CHANNELS(tag) \
    (g_str_equal((tag), GNAC_TAG_CHANNELS))
#define LIBGNAC_METADATA_TAG_IS_COMMENT(tag) \
    (g_str_equal((tag), GST_TAG_COMMENT))
#define LIBGNAC_METADATA_TAG_IS_CONTAINER(tag) \
    (g_str_equal((tag), GST_TAG_CONTAINER_FORMAT))
#define LIBGNAC_METADATA_TAG_IS_DATE(tag) \
    (g_str_equal((tag), GST_TAG_DATE))
#define LIBGNAC_METADATA_TAG_IS_DURATION(tag) \
    (g_str_equal((tag), GST_TAG_DURATION))
#define LIBGNAC_METADATA_TAG_IS_ENCODER(tag) \
    (g_str_equal((tag), GST_TAG_ENCODER))
#define LIBGNAC_METADATA_TAG_IS_FILE_SIZE(tag) \
    (g_str_equal((tag), GNAC_TAG_FILE_SIZE))
#define LIBGNAC_METADATA_TAG_IS_FILENAME(tag) \
    (g_str_equal((tag), GNAC_TAG_FILENAME))
#define LIBGNAC_METADATA_TAG_IS_FRAMERATE(tag) \
    (g_str_equal((tag), GNAC_TAG_FRAMERATE))
#define LIBGNAC_METADATA_TAG_IS_GENRE(tag) \
    (g_str_equal((tag), GST_TAG_GENRE))
#define LIBGNAC_METADATA_TAG_IS_HAS_AUDIO(tag) \
    (g_str_equal((tag), GNAC_TAG_HAS_AUDIO))
#define LIBGNAC_METADATA_TAG_IS_HAS_VIDEO(tag) \
    (g_str_equal((tag), GNAC_TAG_HAS_VIDEO))
#define LIBGNAC_METADATA_TAG_IS_HEIGHT(tag) \
    (g_str_equal((tag), GNAC_TAG_HEIGHT))
#define LIBGNAC_METADATA_TAG_IS_IMAGE(tag) \
    (g_str_equal((tag), GST_TAG_IMAGE))
#define LIBGNAC_METADATA_TAG_IS_INTERLACED(tag) \
    (g_str_equal((tag), GNAC_TAG_INTERLACED))
#define LIBGNAC_METADATA_TAG_IS_LOCATION(tag) \
    (g_str_equal((tag), GST_TAG_LOCATION))
#define LIBGNAC_METADATA_TAG_IS_MODE(tag) \
    (g_str_equal((tag), GNAC_TAG_MODE))
#define LIBGNAC_METADATA_TAG_IS_RATE(tag) \
    (g_str_equal((tag), GNAC_TAG_RATE))
#define LIBGNAC_METADATA_TAG_IS_TITLE(tag) \
    (g_str_equal((tag), GST_TAG_TITLE))
#define LIBGNAC_METADATA_TAG_IS_TRACK_COUNT(tag) \
    (g_str_equal((tag), GST_TAG_TRACK_COUNT))
#define LIBGNAC_METADATA_TAG_IS_TRACK_GAIN(tag) \
    (g_str_equal((tag), GST_TAG_TRACK_GAIN))
#define LIBGNAC_METADATA_TAG_IS_TRACK_NUMBER(tag) \
    (g_str_equal((tag), GST_TAG_TRACK_NUMBER))
#define LIBGNAC_METADATA_TAG_IS_TRACK_PEAK(tag) \
    (g_str_equal((tag), GST_TAG_TRACK_PEAK))
#define LIBGNAC_METADATA_TAG_IS_VBR(tag) \
    (g_str_equal((tag), GNAC_TAG_VBR))
#define LIBGNAC_METADATA_TAG_IS_VIDEO_CODEC(tag) \
    (g_str_equal((tag), GST_TAG_VIDEO_CODEC))
#define LIBGNAC_METADATA_TAG_IS_VIDEO_DEPTH(tag) \
    (g_str_equal((tag), GST_TAG_VIDEO_DEPTH))
#define LIBGNAC_METADATA_TAG_IS_WIDTH(tag) \
    (g_str_equal((tag), GNAC_TAG_WIDTH))

#define LIBGNAC_METADATA_TAG_ALBUM(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ALBUM))
#define LIBGNAC_METADATA_TAG_ALBUM_GAIN(tag) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ALBUM_GAIN))
#define LIBGNAC_METADATA_TAG_ALBUM_PEAK(tag) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ALBUM_PEAK))
#define LIBGNAC_METADATA_TAG_ALBUM_VOLUME_COUNT(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ALBUM_VOLUME_COUNT))
#define LIBGNAC_METADATA_TAG_ALBUM_VOLUME_NUMBER(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ALBUM_VOLUME_NUMBER))
#define LIBGNAC_METADATA_TAG_ARTIST(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ARTIST))
#define LIBGNAC_METADATA_TAG_AUDIO_CODEC(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_AUDIO_CODEC))
#define LIBGNAC_METADATA_TAG_AUDIO_DEPTH(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_AUDIO_DEPTH))
#define LIBGNAC_METADATA_TAG_BITRATE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_BITRATE))
#define LIBGNAC_METADATA_TAG_CHANNELS(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_CHANNELS))
#define LIBGNAC_METADATA_TAG_COMMENT(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_COMMENT))
#define LIBGNAC_METADATA_TAG_CONTAINER_FORMAT(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_CONTAINER_FORMAT))
#define LIBGNAC_METADATA_TAG_DATE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_DATE))
#define LIBGNAC_METADATA_TAG_DURATION(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_DURATION))
#define LIBGNAC_METADATA_TAG_ENCODER(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_ENCODER))
#define LIBGNAC_METADATA_TAG_FILE_SIZE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_FILE_SIZE))
#define LIBGNAC_METADATA_TAG_FILENAME(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_FILENAME))
#define LIBGNAC_METADATA_TAG_FRAMERATE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_FRAMERATE))
#define LIBGNAC_METADATA_TAG_GENRE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_GENRE))
#define LIBGNAC_METADATA_TAG_HAS_AUDIO(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_HAS_AUDIO))
#define LIBGNAC_METADATA_TAG_HAS_VIDEO(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_HAS_VIDEO))
#define LIBGNAC_METADATA_TAG_HEIGHT(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_HEIGHT))
#define LIBGNAC_METADATA_TAG_IMAGE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_IMAGE))
#define LIBGNAC_METADATA_TAG_INTERLACED(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_INTERLACED))
#define LIBGNAC_METADATA_TAG_LOCATION(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_LOCATION))
#define LIBGNAC_METADATA_TAG_MODE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_MODE))
#define LIBGNAC_METADATA_TAG_RATE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_RATE))
#define LIBGNAC_METADATA_TAG_TITLE(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_TITLE))
#define LIBGNAC_METADATA_TAG_TRACK_COUNT(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_TRACK_COUNT))
#define LIBGNAC_METADATA_TAG_TRACK_GAIN(tag) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_TRACK_GAIN))
#define LIBGNAC_METADATA_TAG_TRACK_NUMBER(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_TRACK_NUMBER))
#define LIBGNAC_METADATA_TAG_TRACK_PEAK(tag) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_TRACK_PEAK))
#define LIBGNAC_METADATA_TAG_VBR(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_VBR))
#define LIBGNAC_METADATA_TAG_VIDEO_CODEC(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_VIDEO_CODEC))
#define LIBGNAC_METADATA_TAG_VIDEO_DEPTH(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GST_TAG_VIDEO_DEPTH))
#define LIBGNAC_METADATA_TAG_WIDTH(tags) \
    (g_hash_table_lookup((GHashTable *) (tags), GNAC_TAG_WIDTH))

G_END_DECLS

#endif /* __LIBGNAC_METADATA_TAGS_H__ */
