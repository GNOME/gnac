/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 *
 *  NOTES: log idea stolen from rhythmbox
 */ 

#ifndef __LIBGNAC_DEBUG_H__
#define __LIBGNAC_DEBUG_H__

#include <glib.h>

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define libgnac_debug(...) \
        libgnac_debug_real (__func__, __FILE__, __LINE__, TRUE, __VA_ARGS__)
#elif defined(__GNUC__) && __GNUC__ >= 3
#define libgnac_debug(...) \
        libgnac_debug_real (__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define libgnac_debug
#endif

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define libgnac_warning(...) \
        libgnac_warning_real (__func__, __FILE__, __LINE__, TRUE, __VA_ARGS__)
#elif defined(__GNUC__) && __GNUC__ >= 3
#define libgnac_warning(...) \
        libgnac_warning_real (__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define libgnac_warning
#endif

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define libgnac_critical(...) \
        libgnac_critical_real (__func__, __FILE__, __LINE__, TRUE, __VA_ARGS__)
#elif defined(__GNUC__) && __GNUC__ >= 3
#define libgnac_critical(...) \
        libgnac_critical_real (__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define libgnac_critical
#endif

G_BEGIN_DECLS

extern gboolean LIBGNAC_DEBUG;
extern gboolean LIBGNAC_VERBOSE;

void 
libgnac_debug_real(const gchar *func,
				           const gchar *file,
				           gint         line,
				           const gchar *format, 
                   ...);

void
libgnac_critical_real(const gchar *func,
				              const gchar *file,
				              gint         line,
				              const gchar *format, 
                      ...);

void
libgnac_warning_real(const gchar *func,
				             const gchar *file,
				             gint         line,
				             const gchar *format, 
                     ...);

void
libgnac_info(const gchar *format, ...);

G_END_DECLS

#endif /* __LIBGNAC_DEBUG_H__ */
