/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 *
 */ 

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gprintf.h>

#include "libgnac-debug.h"


gboolean LIBGNAC_DEBUG;
gboolean LIBGNAC_VERBOSE;

void
libgnac_debug_real(const gchar *func,
                   const gchar *file,
                   const gint   line,
                   const gchar *format, 
                   ...)
{
  if (!LIBGNAC_DEBUG) return;

	va_list args;
	gchar *buffer;

	va_start(args, format);
	gint ret = g_vasprintf(&buffer, format, args);
	va_end(args);

  if (ret != -1) {
    g_printerr("[DEBUG] %s:%d: %s\n", file, line, buffer);
    g_free(buffer);
  }
}


void
libgnac_critical_real(const gchar *func,
                      const gchar *file,
                      const gint   line,
                      const gchar *format,
                      ...)
{
	va_list args;
	gchar *buffer;

	va_start(args, format);
	gint ret = g_vasprintf(&buffer, format, args);
	va_end(args);

  if (ret != -1) {
    g_printerr("[CRITICAL] %s:%d: %s\n", file, line, buffer);
    g_free(buffer);
  }
}


void
libgnac_warning_real(const gchar *func,
                     const gchar *file,
                     const gint   line,
                     const gchar *format,
                     ...)
{
	va_list args;
	gchar *buffer;

	va_start(args, format);
	gint ret = g_vasprintf(&buffer, format, args);
	va_end(args);

  if (ret != -1) {
    g_printerr("[WARNING] %s:%d: %s\n", file, line, buffer);
    g_free(buffer);
  }
}


void
libgnac_info(const gchar *format, ...)
{
  if (!LIBGNAC_VERBOSE) return;

	va_list args;
	gchar *buffer;

	va_start(args, format);
	gint ret = g_vasprintf(&buffer, format, args);
	va_end(args);

  if (ret != -1) {
    g_print("[INFO] %s\n", buffer);
    g_free(buffer);
  }
}
