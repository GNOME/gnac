/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_MARSHALLERS_H__
#define __LIBGNAC_MARSHALLERS_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:STRING,STRING,POINTER */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_POINTER(
    GClosure     *closure,
    GValue       *return_value,
    guint         n_param_values,
    const GValue *param_values,
    gpointer      invocation_hint,
    gpointer      marshal_data);

/* VOID:UINT,FLOAT,UINT64 */
extern void g_cclosure_user_marshal_VOID__UINT_FLOAT_UINT64(
    GClosure     *closure,
    GValue       *return_value,
    guint         n_param_values,
    const GValue *param_values,
    gpointer      invocation_hint,
    gpointer      marshal_data);

/* BOOLEAN:POINTER */
extern void g_cclosure_user_marshal_BOOLEAN__POINTER(
    GClosure     *closure,
    GValue       *return_value,
    guint         n_param_values,
    const GValue *param_values,
    gpointer      invocation_hint,
    gpointer      marshal_data);

G_END_DECLS

#endif /* __LIBGNAC_MARSHALLERS_H__ */
