/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_ERROR_H__
#define __LIBGNAC_ERROR_H__

#include <glib.h>
#include "libgnac-media-item.h"

#define LIBGNAC_ERROR libgnac_error_quark()

G_BEGIN_DECLS

typedef enum 
{
  LIBGNAC_ERROR_MISSING_PLUGIN, 
  LIBGNAC_ERROR_MISSING_FILE, 
  LIBGNAC_ERROR_SAME_FILE,
  LIBGNAC_ERROR_FILE_EXISTS,
  LIBGNAC_ERROR_INVALID_FILENAME,
  LIBGNAC_ERROR_UNABLE_CREATE_OUTPUT,
  LIBGNAC_ERROR_PIPELINE_CREATION

} LibgnacError;

GQuark libgnac_error_quark(void) G_GNUC_CONST;

void
libgnac_error_handle_missing_plugin(LibgnacMediaItem *item,
                                    const GError     *error);

gboolean
libgnac_error_handle_dest_file_already_exists(LibgnacMediaItem *item,
                                              GstMessage       *message,
                                              gchar            *uri);

G_END_DECLS

#endif /* __LIBGNAC_ERROR_H__ */
