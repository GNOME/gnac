/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_METADATA_H__
#define __LIBGNAC_METADATA_H__

#include <gio/gio.h>
#include <glib.h>

#include "libgnac-metadata-tags.h"

G_BEGIN_DECLS

#define LIBGNAC_TYPE_METADATA (libgnac_metadata_get_type())
#define LIBGNAC_METADATA(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
    LIBGNAC_TYPE_METADATA, LibgnacMetadata))
#define LIBGNAC_METADATA_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), \
    LIBGNAC_TYPE_METADATA, LibgnacMetadataClass))
#define LIBGNAC_IS_METADATA(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), \
    LIBGNAC_TYPE_METADATA))
#define LIBGNAC_IS_METADATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
    LIBGNAC_TYPE_METADATA))
#define LIBGNAC_METADATA_GET_CLASS(obj) (G_TYPE_CHECK_CLASS_CAST((obj), \
    LIBGNAC_TYPE_METADATA, LibgnacMetadataClass))

typedef struct LibgnacMetadataPrivate LibgnacMetadataPrivate;

typedef struct 
{
  GObject parent;
  LibgnacMetadataPrivate *priv;

} LibgnacMetadata;

typedef struct
{
  GObjectClass parent_class;

} LibgnacMetadataClass;


LibgnacMetadata *
libgnac_metadata_new(void);

GType
libgnac_metadata_get_type(void);

gboolean
libgnac_metadata_remove(LibgnacMetadata  *md,
                        GFile            *uri);

void
libgnac_metadata_remove_all(LibgnacMetadata  *md);

/*
 * The returned LibgnacTags must be freed with 
 * libgnac_metadata_tags_free.
 */
LibgnacTags *
libgnac_metadata_extract(LibgnacMetadata  *md,
                         GFile            *uri,
                         GError          **error);

void
libgnac_metadata_tags_free(LibgnacTags *tags);

gboolean
libgnac_metadata_tag_exists(LibgnacTags *tags,
                            const gchar *name);

gboolean
libgnac_metadata_tags_exist(LibgnacTags *tags, ...);

LibgnacTags *
libgnac_metadata_get_dummy_tags(void);

G_END_DECLS

#endif /* __LIBGNAC_METADATA_H__ */
