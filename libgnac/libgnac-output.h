/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_OUTPUT_H__
#define __LIBGNAC_OUTPUT_H__

#include <gio/gio.h>
#include <glib.h>

#define RENAME_PATTERN_SEPARATOR '%'

#define RENAME_PATTERN_ARTIST       'a'
#define RENAME_PATTERN_ALBUM        'b'
#define RENAME_PATTERN_COMMENT      'c'
#define RENAME_PATTERN_DATE         'y'
#define RENAME_PATTERN_DISC_NUMBER  'd'
#define RENAME_PATTERN_DISC_COUNT   'e'
#define RENAME_PATTERN_FILENAME     'f'
#define RENAME_PATTERN_GENRE        'g'
#define RENAME_PATTERN_TITLE        't'
#define RENAME_PATTERN_TRACK_NUMBER 'n'
#define RENAME_PATTERN_TRACK_COUNT  'l'

/* default values (if a given tag does not exist, the folder 
 * hierarchy will be created using those values) */
#define RENAME_PATTERN_DEFAULT_ARTIST     N_("Unknown Artist")
#define RENAME_PATTERN_DEFAULT_ALBUM      N_("Unknown Album")
#define RENAME_PATTERN_DEFAULT_COMMENT    N_("No Comments")
#define RENAME_PATTERN_DEFAULT_FILENAME   N_("Unknown Filename")
/* Translators: Unknown genre */
#define RENAME_PATTERN_DEFAULT_GENRE      N_("Unknown")
#define RENAME_PATTERN_DEFAULT_TITLE      N_("Unknown Title")
/* "numerical" values */
#define RENAME_PATTERN_DEFAULT_DISC_NUMBER  "1"
#define RENAME_PATTERN_DEFAULT_DATE         "0000"
#define RENAME_PATTERN_DEFAULT_TRACK_NUMBER "00"

#define LIBGNAC_RENAME_PATTERN_GET_ID(o) \
        (((LibgnacRenamePattern*)(o))->id)
#define LIBGNAC_RENAME_PATTERN_GET_PATTERN(o) \
        (g_regex_get_pattern(((LibgnacRenamePattern*)(o))->regex))
#define LIBGNAC_RENAME_PATTERN_GET_REGEX(o) \
        (((LibgnacRenamePattern*)(o))->regex)

G_BEGIN_DECLS

typedef struct {
  GRegex *regex;
  gchar  *replace;
  gchar   id; /* single letter representing the pattern */
} LibgnacRenamePattern;

typedef enum 
{
  FOLDER_CURRENT,
  FOLDER_SUBDIRECTORY,
  FOLDER_SELECTED,
  FOLDER_TYPE_N
} GnacFolderId;

typedef struct {
  gchar        *extension;
  gboolean      strip_special;
  gchar        *folder_hierarchy;
  gchar        *folder_path;
  gchar        *rename_pattern;
  GnacFolderId  folder_type;
} LibgnacOutputConfig;


void
libgnac_output_finalize(void);

GFile *
libgnac_output_build_output(GFile                *source, 
                            LibgnacOutputConfig  *config,
                            GError              **error);

gchar *
libgnac_output_get_preview_from_pattern(const gchar *pattern,
                                        gboolean     strip_special);

gboolean
libgnac_output_overwrite_dest_file(LibgnacMediaItem *item,
                                   const gchar      *uri);

G_END_DECLS

#endif /* __LIBGNAC_OUTPUT_H__ */
