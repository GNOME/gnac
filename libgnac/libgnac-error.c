/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "libgnac-converter.h"
#include "libgnac-debug.h"
#include "libgnac-error.h"

GQuark
libgnac_error_quark(void)
{
  return g_quark_from_static_string("libgnac-error-quark");
}


void
libgnac_error_handle_missing_plugin(LibgnacMediaItem *item,
                                    const GError     *error)
{
  GstMessage *msg = gst_missing_element_message_new(item->pipeline,
      error->message);
  g_return_if_fail(msg);

  gchar *detail = gst_missing_plugin_message_get_installer_detail(msg);
  gst_message_unref(msg);
  if (!detail) return;

  gchar *details[] = { detail, NULL };
  GstInstallPluginsReturn ret = gst_install_plugins_async(details, NULL,
      libgnac_converter_missing_plugin_result_cb, item->parent);
  g_free(detail);
  if (ret != GST_INSTALL_PLUGINS_STARTED_OK) {
    libgnac_warning("Could not launch installer for required plugins");
  } else {
    libgnac_converter_emit_plugin_install(item->parent);
  }
}


static gboolean
libgnac_error_handle_overwrite(LibgnacMediaItem *item,
                               const gchar      *uri)
{
  libgnac_debug("Destination file already exists");

  gboolean overwrite = FALSE;

  g_signal_emit(item->parent, signals[OVERWRITE], 0,
      item->destination, &overwrite);
  if (overwrite) {
    overwrite = libgnac_output_overwrite_dest_file(item, uri);
  }

  return overwrite;
}


static void
libgnac_error_emit_dest_file_exists_error(LibgnacMediaItem *item,
                                          const gchar      *msg,
                                          const gchar      *uri)
{
  GError *err = NULL;
  libgnac_debug(msg);
  g_set_error_literal(&err, LIBGNAC_ERROR, LIBGNAC_ERROR_FILE_EXISTS, msg);
  g_signal_emit(item->parent, signals[ERROR], 0, uri, msg, err);
  g_clear_error(&err);
}


gboolean
libgnac_error_handle_dest_file_already_exists(LibgnacMediaItem *item,
                                              GstMessage       *message,
                                              gchar            *uri)
{
  gchar *dst_path = g_file_get_path(item->destination);
  if (!g_file_test(dst_path, G_FILE_TEST_EXISTS)) {
    g_free(dst_path);
    return TRUE;
  }

  gchar *src_path = g_file_get_path(item->source);
  if (g_strcmp0(dst_path, src_path) == 0) {
    const gchar *msg = _("Source and destination files are identical");
    libgnac_error_emit_dest_file_exists_error(item, msg, uri);
    g_free(dst_path);
    g_free(src_path);
    return TRUE;
  }

  g_free(src_path);

  if (!libgnac_error_handle_overwrite(item, uri)) {
    const gchar *msg = _("Destination file already exists");
    libgnac_error_emit_dest_file_exists_error(item, msg, uri);
    g_free(dst_path);
    return TRUE;
  }

  g_free(dst_path);

  return FALSE;
}
